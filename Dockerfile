FROM rancher/cli2:v2.6.9

RUN wget -q -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/`wget -qO- https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && chmod +x /usr/local/bin/kubectl

COPY entrypoint /

RUN chmod +x /entrypoint

ENTRYPOINT ["/entrypoint"]
CMD ["rancher"]
