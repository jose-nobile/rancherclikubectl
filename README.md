# rancherclikubectl

rancher/cli2 + kubectl in a docker image of just 66.17 MB (21.29 MB compressed)

If you don't find the latest version, please file a bug, it will be updated soon.


License

Licensed under GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.